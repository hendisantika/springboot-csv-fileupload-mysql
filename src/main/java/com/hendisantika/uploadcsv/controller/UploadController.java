package com.hendisantika.uploadcsv.controller;

import com.hendisantika.uploadcsv.entity.Student;
import com.hendisantika.uploadcsv.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-csv-fileupload-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/09/18
 * Time: 07.27
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/students")
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @Autowired
    UploadController(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @PostMapping("/upload")
    public List<Student> uploadFile(@RequestPart(value = "file") MultipartFile multiPartFile) throws IOException {
        return uploadService.uploadFile(multiPartFile);
    }

    @GetMapping
    public List<Student> getStudents() {
        return uploadService.getStudents();
    }
}