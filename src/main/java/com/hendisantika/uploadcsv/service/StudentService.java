package com.hendisantika.uploadcsv.service;

import com.hendisantika.uploadcsv.entity.Student;
import com.hendisantika.uploadcsv.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-csv-fileupload-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/09/18
 * Time: 07.18
 * To change this template use File | Settings | File Templates.
 */

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public void batchStore(List<Student> studentList) {
        studentList.forEach(student -> studentRepository.save(student));
    }

    public List<Student> getStudents() {
        return studentRepository.findAll();
    }
}
