package com.hendisantika.uploadcsv.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.opencsv.bean.CsvBindByName;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-csv-fileupload-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/09/18
 * Time: 07.13
 * To change this template use File | Settings | File Templates.
 */
@Entity
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int studentId;

    @CsvBindByName
    private String firstName;

    @CsvBindByName
    private String middleName;

    @CsvBindByName
    private String lastName;

    private String email;

    @CsvBindByName
    private String phoneNumber;

    @CsvBindByName
    private Character gender;

    @CsvBindByName
    private String information;

    @CsvBindByName
    private Date dob;

}