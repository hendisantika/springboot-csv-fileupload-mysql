package com.hendisantika.uploadcsv.service;

import com.hendisantika.uploadcsv.entity.Student;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-csv-fileupload-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/09/18
 * Time: 07.28
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UploadService {

    @Autowired
    private StudentService studentService;

    public List<Student> uploadFile(MultipartFile multipartFile) throws IOException {

        File file = convertMultiPartToFile(multipartFile);

        List<Student> mandatoryMissedList = new ArrayList<Student>();

        try (Reader reader = new FileReader(file)) {
            @SuppressWarnings("unchecked")
            CsvToBean<Student> csvToBean = new CsvToBeanBuilder<Student>(reader).withType(Student.class)
                    .withIgnoreLeadingWhiteSpace(true).build();
            List<Student> studentList = csvToBean.parse();

            Iterator<Student> studentListClone = studentList.iterator();

            while (studentListClone.hasNext()) {

                Student student = studentListClone.next();

                if (student.getPhoneNumber() == null || student.getPhoneNumber().isEmpty()
                        || student.getFirstName() == null || student.getFirstName().isEmpty()) {
                    mandatoryMissedList.add(student);
                    studentListClone.remove();
                }
            }

            studentService.batchStore(studentList);
        }
        return mandatoryMissedList;
    }

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public List<Student> getStudents() {
        return studentService.getStudents();
    }
}